/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import Business.ProductDirectory;
import java.text.SimpleDateFormat;
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 *
 * @author harshalneelkamal
 */
public class Customer extends User {

//    private ProductDirectory directory;
//    
//    public ProductDirectory getDirectory() {
//        return directory;
//    }
//
//    public void setDirectory(ProductDirectory directory) {
//        this.directory = directory;
//    }

    public Customer(String password, String userName, String role) {
        super(password, userName, "CUSTOMER");
      //  directory = new ProductDirectory();
    }

    public int compareTo(Customer c) {
        return c.getUserName().compareTo(this.getUserName());
    }

    @Override
    public String toString() {
        return getUserName(); //To change body of generated methods, choose Tools | Templates.
    }
    
    public boolean verify(String password){
        if(password.equals(getPassword()))
            return true;
        return false;
    }  
    
        public String getDatecreated(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	Date date = new Date();
	
        return formatter.format(date);
        }
    
}
