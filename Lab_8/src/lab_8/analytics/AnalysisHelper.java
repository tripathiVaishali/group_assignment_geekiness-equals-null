/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_8.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import lab_8.entities.Comment;
import lab_8.entities.Post;
import lab_8.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {

    public void userWithMostLikes() {
        Map<Integer, Integer> userLikecount = new HashMap<Integer, Integer>();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        for (User user : users.values()) {
            for (Comment c : user.getComments()) {
                int likes = 0;
                if (userLikecount.containsKey(user.getId())) {
                    likes = userLikecount.get(user.getId());
                }
                likes += c.getLikes();
                userLikecount.put(user.getId(), likes);
            }
        }
    }

    public void getFiveMostLikedComment() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<>(comments.values());
        Collections.sort(commentList, new Comparator<Comment>() {
            @Override
            public int compare(Comment o1, Comment o2) {
//so as to get decending list
                return o2.getLikes() - o1.getLikes();
            }
        });
    }
    
    public void postWithMostComments(){
       Map<Integer, Integer> postCommentcount = new HashMap<Integer,Integer>();
       Map<Integer, Post> posts = DataStore.getInstance().getPosts();
       for(Post post : posts.values()){
           for(Comment c : post.getComments()){
               int commenPost = 0;
               if(postCommentcount.containsKey(post.getPostId()))
                   commenPost = postCommentcount.get(post.getPostId());
               commenPost ++;
               postCommentcount.put(post.getPostId(),commenPost);
           }
       }
       int max = 0;
       int maxId = 0;
       for(int id : postCommentcount.keySet()){
           if(postCommentcount.get(id)>max){
               max = postCommentcount.get(id);
               maxId = id;
           }
       }
       System.out.println("Post with Most Comments");
       System.out.println("User ID = "+maxId+" Comments : "+max);
   }
   
        
    
    }
    public void getFiveMostLikedComment(){
        
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();     
        List<Comment> commentList = new ArrayList<>(comments.values());     
        Collections.sort(commentList, new Comparator<Comment>() {     
            @Override         
            public int compare(Comment o1, Comment o2)
            {                 //so as to get decending list        
                return o2.getLikes() - o1.getLikes(); 
    }
    
    public void getTopFiveProActiveandInactiveUsers() {
       Map<Integer, User> users = DataStore.getInstance().getUsers();
       Map<Integer, Post> posts = DataStore.getInstance().getPosts();
       Map<Integer, Integer> userCountDetails = new HashMap<Integer, Integer>();
       for (User user : users.values()) {
           int total = 0;
           //int numberOfComments = 0;
           //numberOfComments = user.getComments().size();
           int numberOfLikes = 0;
           int numberOfPosts = 0;
           for (Comment c : user.getComments()) {
               numberOfLikes = numberOfLikes + c.getLikes();
           }
           for (Post post : posts.values()) {
               if(post.getUserId() == user.getId()){
               numberOfPosts++;
               }
           }
           //total = numberOfComments + numberOfLikes + numberOfPosts;
           total = numberOfLikes + numberOfPosts;
           userCountDetails.put(user.getId(), total);
       }
       List<Map.Entry<Integer, Integer>> listmostinactive = new LinkedList<Map.Entry<Integer, Integer>>(userCountDetails.entrySet());
       List<Map.Entry<Integer, Integer>> listmostactive = new LinkedList<Map.Entry<Integer, Integer>>(userCountDetails.entrySet());


    Collections.sort(listmostinactive, new Comparator<Map.Entry<Integer, Integer>>() {
        public int compare(Map.Entry<Integer, Integer> o1,
                Map.Entry<Integer, Integer> o2) {
            return (o1.getValue()).compareTo(o2.getValue());
        }
    });
       System.out.println("Top 5 inactive users based on Overall (comments,posts,likes)");
    int printCount = 0;
    for (Map.Entry<Integer, Integer> entry : listmostinactive) {
        if (printCount == 5) {
            break;
        }
        System.out.println("User ID: " + entry.getKey() + ", Post and Comment Count: " + entry.getValue());
        printCount++;
    }
       Collections.sort(listmostactive, new Comparator<Map.Entry<Integer, Integer>>() {
        public int compare(Map.Entry<Integer, Integer> o2,
                Map.Entry<Integer, Integer> o1) {
            return (o1.getValue()).compareTo(o2.getValue());
        }
    });
       System.out.println("Top 5 active users based on Overall (comments,posts,likes): ");
    int printCount1 = 0;
    for (Map.Entry<Integer, Integer> entry : listmostactive) {
        if (printCount1 == 5) {
            break;
        }
        System.out.println("User ID: " + entry.getKey());
        printCount1++;
    }
   }

 


  
    
    
//1. Find Average number of likes per comment.
//2. Post with most liked comments.
    public void postWithMostLikedComments() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<>(comments.values());

        Collections.sort(commentList, new Comparator<Comment>() {
            @Override
            public int compare(Comment o1, Comment o2) {
                // so as to get descending list
                return o2.getLikes() - o1.getLikes();
            }
        });

        System.out.print("Post with most liked comments: ");
        System.out.println("Post_id: " + commentList.get(0).getPostId() + ", Likes: " + commentList.get(0).getLikes() + ", Comment ID: " + commentList.get(0).getId());
    }

//3. Post with most comments.
//4. Top 5 inactive users based on posts.
//5. Top 5 inactive users based on comments.
    public void topFiveInactiveUserBasedOnComments() {
        System.out.println("Top 5 Inactive Users Based On Their Comments");

        Map<Integer, Integer> comments = new HashMap<Integer, Integer>();

        Map<Integer, User> users = DataStore.getInstance().getUsers();
        for (User user : users.values()) {

            comments.put(user.getId(), user.getComments().size());
            //  System.out.println(comments);

        }
        Set<Entry<Integer, Integer>> set = comments.entrySet();

        List<Entry<Integer, Integer>> list = new ArrayList<Entry<Integer, Integer>>(set);

        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {

            @Override
            public int compare(Entry<Integer, Integer> o1, Entry<Integer, Integer> o2) {

                return o1.getValue().compareTo(o2.getValue());
            }

        });
        for (int i = 0; i < 5; i++) {
            System.out.println("User ID: " + list.get(i).getKey() + "   No. of comments: " + list.get(i).getValue());
        }
    }

//6. Top 5 inactive users overall (comments, posts and likes)
//7. Top 5 proactive users overall (comments, posts and likes)
 

        });
                }
    
    public void getAverageLikesPerComment() {
        int totalLikes = 0;
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<>(comments.values());
        for (int i = 0; i < commentList.size(); i++) {
            totalLikes += commentList.get(i).getLikes();
        }
        int totalComments = comments.size();
        float average = (float) totalLikes / totalComments;
        System.out.println("Average number of likes per comment is " + average);
}
    
    public void userfiveInactivepost(){
      System.out.println("Top 5 inactive users based on posts:");

       Map<Integer, Integer> posts = new HashMap<Integer,Integer>();

       Map<Integer, Post> users = DataStore.getInstance().getPosts();
       for(Post post : users.values()){

               posts.put(post.getUserId(),post.getPostId());

       }
        Set<Entry<Integer, Integer>> set = posts.entrySet();

   List<Entry<Integer, Integer>> list = new ArrayList<Entry<Integer, Integer>>(set);

   Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {

       @Override
       public int compare(Entry<Integer, Integer> o1,Entry<Integer, Integer> o2) {

           return o1.getValue().compareTo(o2.getValue());
       }
});
       for(int i=0;i<5;i++)
            System.out.println("User ID: "+list.get(i).getKey());
        
    }
}

  
